/**
 * @global
 * @typedef {Object} Vector3
 * @property {number} x
 * @property {number} y
 * @property {number} z
 */

/**
 * @global
 * @typedef {Object} Vector2
 * @property {number} x
 * @property {number} y
 */

/**
  * @global
  * @typedef {Object} Mesh
  * @property {Vector3} position
  */