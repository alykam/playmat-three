import {
	LineBasicMaterial,
	MeshBasicMaterial,
	MeshLambertMaterial ,
	VertexColors
} from 'three';

export const tileMat = new MeshLambertMaterial ({
	color: 0x111111,
});

export const cursorMat = new MeshBasicMaterial({
	vertexColors: VertexColors,
});

export const lineMat = new LineBasicMaterial({
	color: 0x000000,
	linewidth: 30
});

export const cellMat = new MeshBasicMaterial({
	transparent: true,
	opacity: 0
});

export const hexMat = new MeshBasicMaterial({
	color: 0x52aeff
});
