export const CELL = 'CELL';
export const HEX = 'HEX';
export const TILE = 'TILE';
export const CURSOR = 'CURSOR';
export const SQRT3 = Math.sqrt(3);
export const CELL_SIZE = 100;
export const GRID_SIZE = 8;

export const TILE_HEIGHT = 30;
export const TILE_BEVEL = 10;
export const TILE_DEPTH_STEP = 20;
export const TILE_EXTRUDE_SETTINGS = (depth = 1) => ({
	steps: 2,
	depth: (!depth || depth<1) ? TILE_DEPTH_STEP : (TILE_DEPTH_STEP * depth),
	bevelEnabled: true,
	bevelThickness: TILE_DEPTH_STEP/2,
	bevelSize: TILE_DEPTH_STEP/2,
	bevelOffset: -2,
	bevelSegments: 1
});
