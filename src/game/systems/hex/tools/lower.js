export function lower(grid, toolOptions) {
	const cursor = grid.cursor;
	if (cursor.visible) {
		cursor.forEachCell(({cell, gridPoint, tile}) => {
			if(cell.visible) {
				if (tile) {
					let h = cell.strength;
					tile.setHeight(tile.height - h * 15);
				} else if(toolOptions && toolOptions.createTileIfEmpty){				
					tile = grid.addTile(gridPoint.x, gridPoint.y);						
				}
			}
		});
		cursor.move();
	}
}

export default {
	name: 'Lower',
	tool: lower,
	hue: 0.5,
	curve: [0,0,0,0,1,1,1,1],
};
