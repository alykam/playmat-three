import raise from './raise';
import lower from './lower';

export default class Tools{
	toolOptions = {
		createTileIfEmpty: true,
	}

	constructor(grid) {
		this.grid = grid;
		this._tools = {
			lower,
			raise,
		};
		this.selectTool('raise');
	}

	get tools() {
		return Object.keys(this._tools).map(id => ({id, name:this._tools[id].name}));
	}

	selectTool(toolid) {
		const t = this._tools[toolid];
		this.selectedTool = t.tool;
		this.grid.cursor.hue = t.hue;
		this.grid.cursor.setColors();
		this.grid.cursor.strengthCurve = t.curve;
	}

	useTool() {
		this.selectedTool(this.grid, this.toolOptions);
	}
}