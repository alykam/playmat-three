export function raise(grid, toolOptions) {
	const cursor = grid.cursor;
	if (cursor.visible) {
		cursor.forEachCell(({cell, gridPoint, tile}) => {
			if(cell.visible) {
				if (tile) {
					let h = cell.strength;					
					tile.setHeight(tile.height + h*15);
				} else if(toolOptions && toolOptions.createTileIfEmpty){				
					tile = grid.addTile(gridPoint.x, gridPoint.y);						
				}				
			}
		});
		cursor.move();
	}
}

export default {
	name: 'Raise',
	tool: raise,
	hue: 1.0,
	curve: [0,0,0,0,1,1,1,1],
};
