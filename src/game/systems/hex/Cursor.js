import * as THREE from 'three';
import {
	cursorMat
} from '../materials/default';
import {
	CELL_SIZE,
	CURSOR,
} from './constants';
import {
	closestIndex,
	getPositionForPoint,
	getGridFromSize,
	getGridRingFromSize
} from './helpers';
import Hex from './Hex';


export default class Cursor {
	/** @type {number}*/
	_radius = 0;
	/** @type {Mesh}*/
	mesh;
	/** @type {number}*/
	x = 0;
	/** @type {number}*/
	y = 0;
	/** @type {number[]}*/
	_strengthCurve;
	/** @type {Hex[]}*/
	cells = [];
	/** @type {boolean}*/
	visible = false;
	/** @type {number}*/
	hue = 1;
	/** @type {boolean} - limit or not the cursor to the grid. If {false} and the radius is bigger than 1, the outter selection can reach outside the grid */
	limitCursorToGrid = false;

	strengthGeos = {}

	/**
	 * @function strengthFunction
	 * @param {number} i - number between 1 and 0
	 * @returns {number[]}
	 */
	strengthFunction = (i) => {
		const calc0 = (1 - i);
		const calc1 = Math.pow(calc0, 3);
		const calc2 = 3 * i * Math.pow(calc0, 2);
		const calc3 = 3 * (Math.pow(i, 2)) * calc0;
		const calc4 = Math.pow(i, 3);
		const s = this._strengthCurve;
		const x1 = s[0];
		const x2 = s[2];
		const x3 = s[4];
		const x4 = s[6];

		const y1 = s[1];
		const y2 = s[3];
		const y3 = s[5];
		const y4 = s[7];

		const x = Number.parseInt(((calc1 * x1) + (calc2 * x2) + (calc3 * x3) + (calc4 * x4))*10)/10;
		const y = Number.parseInt(((calc1 * y1) + (calc2 * y2) + (calc3 * y3) + (calc4 * y4))*10)/10;
		return [x, y];
	}

	/**
	 * @param {number[]} curve
	 */
	set strengthCurve(curve) {
		this._strengthCurve = curve;
		this.curvePoints = [];
		for (let i = 0; i < 1; i += 0.1) {
			this.curvePoints.push(this.strengthFunction(i));
		}
	}

	get strengthCurve() {
		return this._strengthCurve;
	}

	constructor(initialRadius = 1, size = CELL_SIZE, grid) {
		this.grid = grid;
		this.cursor = new THREE.Group();
		this.cursor.visible = false;
		this.strengthCurve = [0,0,.3, .5, 1, 1, 1, 1];
		this.radius = initialRadius;
		this.size = size;
	}


	addHex = (x, y, s) => {
		if (!this.cells[x]) {
			this.cells[x] = new Array(this.radius * 2);
		}
		const hex = new Hex({
			hasLine: false,
			material: cursorMat,
			type: CURSOR,
		});

		hex.init();

		const position = getPositionForPoint(x, y, this.size);
		hex.mesh.position.copy(position);
		hex.strength = s;
		hex.x = x;
		hex.y = y;
		this.cells[x][y] = hex;
		this.cursor.add(hex.mesh);
	}

	/** 
	 * Radius setter; if the radius increases, it adds the new outter rings hexes to the cursor.
	 * If it deacreses, it sets the visibility of the outter hexes to false, 
	 * Preventing recreating meshes when the size grows again.
	 * 
	 * @param {number} r 
	 */
	set radius(r) {
		if (r > this._radius) {
			getGridRingFromSize(this._radius, r - 1, (x, y) => {
				const cIndex = closestIndex(Math.max(Math.abs(x), Math.abs(y), Math.abs(x + y)) / r, this.curvePoints.map(p => p[0]));
				this.addHex(x, y, 1 - this.curvePoints[cIndex][1]);
			});
			this._radius = r;
			this.setColors();
		} else if (r < this._radius) {
			//
		}
		this._radius = r;
	}

	get radius() {
		return this._radius;
	}

	get cursorPoints() {
		return getGridFromSize(this.radius - 1, undefined, [this.x, this.y]);
	}

	setColors = () => {
		const color = new THREE.Color();
		this.forEachCell(({cell}) => {
			if (!this.strengthGeos[cell.strength]) {
				this.strengthGeos[cell.strength] = cell.mesh.geometry.clone();
			}
			if (cell.mesh.geometry !== this.strengthGeos[cell.strength]) {
				cell.mesh.geometry = this.strengthGeos[cell.strength];
			}
			color.setHSL(this.hue, cell.strength, .5);
			const colors = [];
			cell.mesh.geometry.getAttribute('position').array.forEach(() => {
				colors.push(color.r, color.g, color.b);
			});
			cell.mesh.geometry.addAttribute('color', new THREE.Float32BufferAttribute(colors, 3));
			cell.mesh.geometry.colorsNeedUpdate = true;
			cell.mesh.geometry.elementsNeedUpdate = true;
		});
	}

	forEachCell(func) {
		if(!func) {
			return;
		}
		xLoop:
		for(let cx of Object.keys(this.cells)){
			for(let cy of Object.keys(this.cells[cx])) {
				cx = Number.parseInt(cx, 10);
				cy = Number.parseInt(cy, 10);
				if(Math.abs(cx) >= this.radius) {
					continue xLoop;					
				}
				if(Math.abs(cy) >= this.radius) {
					continue;
				}
				const cell = this.cells[cx][cy];
				const cz = cell.z;
				const params = {
					cell:this.cells[cx][cy],
					localPoint: new THREE.Vector3(cx, cy, cz),
					gridPoint: new THREE.Vector2(cx + this.x, cy+this.y),
					elevation: this.elevation,					
				};
				const {tiles} = this.grid;
				if(tiles[cx + this.x] && tiles[cx + this.x][cy + this.y]){
					params.tile = tiles[cx + this.x][cy + this.y];
				}
				func(params);
			}
		}
	}

	/**
	 * Hides the cursor
	 */
	hide() {
		this.visible = false;
		this.cursor.visible = false;
	}
	/**
	 * Shows the cursor
	 */
	show() {
		this.visible = true;
		this.cursor.visible = true;
	}
	/**
	 * Moves the cursor to the new x, y and z position. It uses the grid referece to limit 
	 * the movent to the grid, repositioning the cursor center if the cursor hits the borders
	 * 
	 * @param {number} x 
	 * @param {number} y 
	 * @param {number} z 
	 */
	move(x = this.x, y = this.y, z = 0) {
		// if (this.limitCursorToGrid) {
		// 	const s = () => -(-x - y);
		// 	if (x > 0 && x + this.radius > this.grid.size) {
		// 		x -= (x + this.radius - 1 - this.grid.size);
		// 	}
		// 	if (y > 0 && y + this.radius > this.grid.size) {
		// 		y -= (y + this.radius - 1 - this.grid.size);
		// 	}
		// 	if (x < 0 && x - this.radius < -this.grid.size) {
		// 		x -= (x - this.radius + 1 + this.grid.size);
		// 	}
		// 	if (y < 0 && y - this.radius < -this.grid.size) {
		// 		y -= (y - this.radius + 1 + this.grid.size);
		// 	}
		// 	if (s() > 0 && s() + this.radius > this.grid.size) {
		// 		x += (-s() - this.radius + 1 + this.grid.size);
		// 		y += (-s() - this.radius + 1 + this.grid.size);
		// 	}
		// 	if (s() < 0 && s() - this.radius < -this.grid.size) {
		// 		x -= (s() - this.radius + 1 + this.grid.size);
		// 		y -= (s() - this.radius + 1 + this.grid.size);
		// 	}
		// } 

		this.x = x;
		this.y = y;
		this.z = z;

		const position = getPositionForPoint(x, y, this.size);
		this.cursor.position.copy(position);
		this.elevation = [];
		this.forEachCell(({cell, localPoint, tile}) => {
			const l = localPoint;
			const dx = l.x + x;
			const dy = l.y + y;
			if (Math.abs(dx) > this.grid.size
			|| Math.abs(dy) > this.grid.size
			|| Math.abs(-dx - dy) > this.grid.size) {
				cell.hide();
				return;
			} else {
				cell.show();
			}
			let cz = 0;
			if(tile){
				cz = tile.getZ();
			} 
			this.elevation.push(cz);		
			cell.mesh.position.z = cz + 20;
		});		
		this.elevation.sort();
	}
}