import {
	EdgesGeometry,
	LineSegments
} from 'three';
import Hex from './Hex';

import {
	cellMat,
	lineMat,
} from '../materials/default';
import {
	CELL,
} from './constants';
import {
	
} from './helpers';

export default class Cell extends Hex{
	_height = 0
	type = CELL;
	hasLine = true;
	material = cellMat;

	getZ() {
		return this.height;
	}

	constructor(args={}) {
		super(args);
		this.material = cellMat;
	}

	init() {
		super.init();
		this.generateLines();
	}

	onMouseEnter = () => {
		if (!this.isMouseOver) {
			if (this.grid) {
				const c = this.grid.cursor;
				if (c) {
					if (!c.visible) {
						c.show();
					}
					c.move(this.x, this.y, this.z);
				}
			}
			this.isMouseOver = true;
		}
	}

	onMouseLeave = () => {
		if (this.isMouseOver) {						
			this.isMouseOver = false;
		}
	}

	generateLines() {
		this.lineGeometry = new EdgesGeometry(this.geometry); // or WireframeGeometry
	
		this.wireframe = new LineSegments(this.lineGeometry, lineMat);
		this.mesh.add(this.wireframe);
	}
}
