import {
	TILE,
} from './constants';
import {tileMat} from '../materials/default';
import {
	getHexTile
} from './helpers';

import Cell from './Cell';

export default class Tile extends Cell{
	constructor(args) {
		super(args);
		this.type = TILE;
		this.material = tileMat;
		this.height = 30;
	}
	
	init() {
		super.init();
		this.mesh.userData.onMouseEnter = this.onMouseEnter;
		this.mesh.userData.onMouseLeave = this.onMouseLeave;
	}

	setHeight(h) {
		if(h>20) {
			this.height = h;

			let a = this.mesh.geometry.getAttribute('position').array;
			for (let i = a.length - 1; i > -1; i -= 3) {
				if (a[i] === 0) {
					continue;
				}
				this.mesh.geometry.attributes.position.set([this.height], i);
			}
			this.mesh.geometry.attributes.position.needsUpdate = true;
			this.height = h;
			
			a = this.wireframe.geometry.getAttribute('position').array;
			for (let i = a.length - 1; i > -1; i -= 3) {
				if (a[i] === 0) {
					continue;
				}
				this.wireframe.geometry.attributes.position.set([this.height], i);
			}
			this.wireframe.geometry.attributes.position.needsUpdate = true;
		}
	}

	setMaterial(material) {
		this.mesh.material = material;
	}

	generateVertices() {
		this.vertices = new Float32Array(getHexTile(this.size, this.height));
		return this.vertices;
	}
}
