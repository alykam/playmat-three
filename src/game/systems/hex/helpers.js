import * as THREE from 'three';

import {
	CELL_SIZE,
	TILE_HEIGHT,
	TILE_BEVEL,
	SQRT3
} from './constants';

// /**
//  * @typedef {Object} Points
//  * @property {number} size
//  * @property {Vector3[]} vertices
//  * @property {Float32BufferAttribute} positions
//  * @property {Shape} cellShape
//  * @property {ShapeGeometry} cellShapeGeometry
//  */

// /**
//  * @type {Points}
//  */
// let points = {};

/**
 * 
 * @param {number} i - current hex vertex (from 0 to 5) 
 * @param {number} size - current cell size
 * @returns {Vector3} - an {@link Vector3} containing the position of the current vertex
 */
export const getVertexPos = (i = 0, size = CELL_SIZE) => {
	var angleDeg = (60 * i) - 30;
	var angleRad = (Math.PI / 180) * angleDeg;
	const c = new THREE.Vector3(0, 0, 0);
	return new THREE.Vector3(
		c.x + size * Math.cos(angleRad),
		c.y + size * Math.sin(angleRad),
		c.z
	);
};


/**
 * Get a hex face of radial triangles from size and z (height)
 * 
 * @param {number} size - current cell size
 * @param {number} z - z position of the face
 * @returns {number[]} an one dimensional array of points (x, y, z) of the hex faces
 */
export const getHexFace = (size = CELL_SIZE, z = 0) => {
	const points = [];
	for (let j = 0; j < 6; j++) {
		points.push(0, 0, z);
		for (let i = 1; i < 3; i++) {
			const p = getVertexPos(i + j, size);
			points.push(p.x, p.y, z);
		}
	}
	return points;
};

/**
 * Calculates the 3d tile triangles
 * 
 * @param {number} size - the radius of the hex
 * @param {number} height - the height of the tile
 * @param {number} bevel - the top bevel, even if it is 0 the bebel triangles are added, so the resize function works without checking if there is a bebel or not. Also it looks good without being expensive.
 * 
 * @returns {Float32Array} of the mesh vertex points
 */
export const getHexTile = (size = CELL_SIZE, height = TILE_HEIGHT, bevel = TILE_BEVEL) => {
	const points = [];
	const topFace = [];
	const bevelFaces = [];
	const walls = [];
	const base = [];
	for(let j=0; j<6;j++) {
		//outter points
		const p1 = getVertexPos(1 + j, size);
		const p2 = getVertexPos(2 + j, size);

		// inner points
		const b1 = getVertexPos(1 + j, size - bevel);
		const b2 = getVertexPos(2 + j, size - bevel);
		
		//base triangle
		base.push(p1.x, p1.y, 0);
		base.push(0, 0, 0); // center
		base.push(p2.x, p2.y, 0);
		//wall t1
		walls.push(p1.x, p1.y, 0);
		walls.push(p2.x, p2.y, 0);
		walls.push(p1.x, p1.y, height - bevel);
		//wall t2
		walls.push(p2.x, p2.y, 0);
		walls.push(p2.x, p2.y, height - bevel);
		walls.push(p1.x, p1.y, height - bevel);
		//bevel t1
		bevelFaces.push(p1.x, p1.y, height - bevel);
		bevelFaces.push(p2.x, p2.y, height - bevel);
		bevelFaces.push(b1.x, b1.y, height);
		//bevel t2
		bevelFaces.push(p2.x, p2.y, height - bevel);
		bevelFaces.push(b2.x, b2.y, height);
		bevelFaces.push(b1.x, b1.y, height);
		//top triangle
		topFace.push(0, 0, height); // center
		topFace.push(b1.x, b1.y, height);
		topFace.push(b2.x, b2.y, height);
	}
	// concatenate points keeping higher last
	points.push(...base, ...walls, ...bevelFaces, ...topFace);
	return points;
};

let cs = CELL_SIZE;
let h = cs * 2 * 0.75;
let w = SQRT3 * cs;
/**
 * 
 * @param {number} x 
 * @param {number} y 
 * @param {number} cellSize 
 */
export function getPositionForPoint(x = 0, y = 0, cellSize = CELL_SIZE) {
	if(cellSize !== cs) {
		cs = cellSize;
		h = cellSize * 2 * 0.75;
		w = SQRT3 * cellSize;
	}
	return new THREE.Vector3(x * w + (y * .5 * w), (y * h), 0);
}


export function setGradient(geometry, colors, axis, reverse) {

	geometry.computeBoundingBox();

	var bbox = geometry.boundingBox;
	var size = new THREE.Vector3().subVectors(bbox.max, bbox.min);

	var vertexIndices = ['a', 'b', 'c'];
	var face, vertex, normalized = new THREE.Vector3(),
		normalizedAxis = 0;

	for (var c = 0; c < colors.length - 1; c++) {

		var colorDiff = colors[c + 1].stop - colors[c].stop;

		for (var i = 0; i < geometry.faces.length; i++) {
			face = geometry.faces[i];
			for (var v = 0; v < 3; v++) {
				vertex = geometry.vertices[face[vertexIndices[v]]];
				normalizedAxis = normalized.subVectors(vertex, bbox.min).divide(size)[axis];
				if (reverse) {
					normalizedAxis = 1 - normalizedAxis;
				}
				if (normalizedAxis >= colors[c].stop && normalizedAxis <= colors[c + 1].stop) {
					var localNormalizedAxis = (normalizedAxis - colors[c].stop) / colorDiff;
					face.vertexColors[v] = colors[c].color.clone().lerp(colors[c + 1].color, localNormalizedAxis);
				}
			}
		}
	}
}

/**
 * 
 * @param {number} size 
 * @param {Function} callback 
 */
export function getGridFromSize(size, callback, center = [0,0]) {
	const grid = [];
	for (var x = -size; x <= size; x++) {
		for (var y = -size; y <= size; y++) {
			const z = -x - y;
			if (z < -size || z > size) {
				continue;
			}
			if (callback) {
				callback(x + center[0], y + center[1]);
			}
			grid.push([x + center[0], y + center[1]]);
		}
	}
	return grid;
}

/**
 * @param {number} start 
 * @param {number} size 
 * @param {Function} callback 
 */
export function getGridRingFromSize(start=0, size, callback) {
	const grid = [];
	for (var x = -size; x <= size; x++) {
		if(x > -start && x < start) {
			continue;
		}
		for (var y = -size; y <= size; y++) {
			if (y > -start && y < start) {
				continue;
			}
			const z = -x - y;
			if (z < -size || z > size) {
				continue;
			}
			if (callback) {
				callback(x, y);
			}
			grid.push([x, y]);
		}
	}
	return grid;
}

export function closest(goal, values) {
	return values.reduce(function (prev, curr) {
		return (Math.abs(curr - goal) < Math.abs(prev - goal) ? curr : prev);
	});
} 

export function closestIndex(goal, values) {
	return values.reduce(function (prev, _curr, i) {
		return (Math.abs(values[i] - goal) < Math.abs(values[prev] - goal) ? i : prev);
	});
}