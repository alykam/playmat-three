import * as THREE from 'three';

import {lineMat, hexMat} from '../materials/default';
import {
	CELL_SIZE,
	HEX,
} from './constants';

import {
	getHexFace
} from './helpers';

export default class Cell {
	type = HEX;
	hasLine = false;
	center = new THREE.Vector3(0, 0, 0)
	height = 0;
	size = CELL_SIZE;
	material = hexMat;
	lineMaterial = lineMat;
	isMouseOver = false;
	grid;

	/**
	 * 
	 * @param {Cell} args 
	 */
	constructor(args = {}) {
		Object.keys(args).forEach(k => this[k] = args[k]);
	}

	/**
	 * Hides the mesh
	 */
	hide() {
		this.visible = false;
		this.mesh.visible = false;
	}
	/**
	 * Shows the mesh
	 */
	show() {
		this.visible = true;
		this.mesh.visible = true;
	}

	init() {	
		this.generateMesh();
	}

	generateVertices() {
		this.vertices = new Float32Array(getHexFace(this.size));
		return this.vertices;
	}	

	generateGeometry() {
		this.geometry = new THREE.BufferGeometry();
			
		this.geometry.addAttribute('position', new THREE.BufferAttribute(this.vertices || this.generateVertices(), 3));
		this.geometry.computeFaceNormals();
		this.geometry.computeVertexNormals();
		this.geometry.dynamic = true;

		return this.geometry;
	}

	generateMesh() {
		this.mesh = new THREE.Mesh(this.geometry || this.generateGeometry(), this.material);
		this.mesh.userData.type = this.type;
		this.mesh.userData.onMouseEnter = this.onMouseEnter;
		this.mesh.userData.onMouseLeave = this.onMouseLeave;
	}
	
}
