import * as THREE from 'three';

import Cell from './Cell';
import Cursor from './Cursor';
import Tile from './Tile';
import Tools from './tools';
import {
	CELL_SIZE,
	GRID_SIZE
} from './constants';

import {
	getPositionForPoint,
	getGridFromSize
} from './helpers';


export default class Grid {
	constructor(size = GRID_SIZE, cellSize = CELL_SIZE) {
		this.tiles = new Array(size);
		this.cells = new Array(size);
		this.grid = new THREE.Group();
		this.cursor = new Cursor(3, cellSize, this);
		this.tilesGroup = new THREE.Group();
		this.tools = new Tools(this);
		this.size = size;
		this.cellSize = cellSize;
		this.init();
	}

	addCell = (x = 0, y = 0) => {
		if (!this.cells[x]) {
			this.cells[x] = new Array(this.size);
		}

		const localCenter = getPositionForPoint(x, y);
		const cell = new Cell({center:localCenter, size:this.cellSize});
		cell.init();
		cell.mesh.position.x = localCenter.x;
		cell.mesh.position.y = localCenter.y;
		cell.mesh.userData.cell = cell;
		cell.grid = this;
		cell.x = x;
		cell.y = y;
		this.cells[x][y] = cell;
		this.grid.add(cell.mesh);
	}

	addTile(x = 0, y = 0, tile) {
		if (!this.tiles[x]) {
			this.tiles[x] = new Array(this.size);
		} else {
			if (this.tiles[x][y]) {
				if(!tile && this.tiles[x][y].height === 1) {
					return;
				}
				this.removeTile(x, y);
			} 
		}
		if(!tile) {
			tile = new Tile();
			tile.init();
		} 
		tile.mesh.position.copy(getPositionForPoint(x, y));
		tile.grid = this;
		tile.x = x;
		tile.y = y;
		this.tiles[x][y] = tile;
		this.tilesGroup.add(tile.mesh);
		return tile;
	}

	removeTile(x, y) {
		if (this.tiles[x] && this.tiles[x][y]) {
			this.tilesGroup.remove(this.tiles[x][y].mesh);
			this.tiles[x][y].mesh.material.dispose();
			delete this.tiles[x][y];
			this.tiles[x][y] = undefined;
		}
	}

	init() {		
		getGridFromSize(this.size, this.addCell);	
	}

}
