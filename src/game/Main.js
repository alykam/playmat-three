import * as THREE from 'three';
import Grid from './systems/hex/Grid';
// import {
// 	OrbitControls
// } from 'three/examples/jsm/controls/OrbitControls.js';

// import {
// 	getHexTile
// } from './systems/hex/helpers';

// import {
// 	tileMat
// } from './systems/materials/default';
// if ( WEBGL.isWebGLAvailable() === false ) {
// 				document.body.appendChild( WEBGL.getWebGLErrorMessage() );
// 			}
var container;
// var stats;
var camera, scene, renderer;
// var controls;
var mesh;
var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();
var mouseDown = false;
var altKey = false;
var tiles;
// var intersecteds = [];
// var extrudeSettings = {
// 	steps: 2,
// 	depth: 20,
// 	bevelEnabled: true,
// 	bevelThickness: 10,
// 	bevelSize: 10,
// 	bevelOffset: -2,
// 	bevelSegments: 1
// };
var grid;
var intersected;
init();
animate();

// var oldX, oldY;

const onMouseDown = () => {
	// if(grid.cursor.visible) {
	// 	console.log(grid.cursor);
	// }
	grid.tools.useTool();	
};

function init() {
	container = document.body;


	camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 10000);
	camera.position.set(2000, -2000, 3300);
	console.log(camera);
	camera.rotation.set(0,1,0);
	camera.lookAt(0, 0, 0);

	// controls = new OrbitControls(camera);
	// controls.update();

	scene = new THREE.Scene();
	scene.background = new THREE.Color(0xf0f0f0);
	var light = new THREE.AmbientLight(0xa0a0a0); // soft white light
	var lights = [];
	lights[0] = new THREE.PointLight(0xffffff, 1, 0);
	lights[1] = new THREE.PointLight(0xffffff, 1, 0);
	lights[2] = new THREE.PointLight(0xffffff, 1, 0);

	lights[0].position.set(0, 200, 3000);
	lights[1].position.set(100, 200, 2000);
	lights[2].position.set(-500, -200, -500);

	scene.add(light);
	scene.add(lights[0]);
	scene.add(lights[1]);
	scene.add(lights[2]);
	// var r = 800;
	grid = new Grid();
	mesh = grid.grid;
	tiles = grid.tilesGroup;
	// const cell = new Cell(new THREE.Vector3(0, 0, 0), 100)
	// mesh = cell.mesh
	// console.log(mesh.children);

	scene.add(mesh);
	scene.add(grid.cursor.cursor);
	scene.add(grid.grid);
	scene.add(tiles);

	// var geometry = new THREE.BufferGeometry();

	// var vertices = new Float32Array(getHexTile());

	// // itemSize = 3 because there are 3 values (components) per vertex
	// geometry.addAttribute('position', new THREE.BufferAttribute(vertices, 3));
	// geometry.computeFaceNormals();
	// geometry.computeVertexNormals();
	// geometry.dynamic = true;

	// mesh = new THREE.Mesh(geometry, tileMat);
	// // var geo = new THREE.EdgesGeometry(mesh.geometry); // or WireframeGeometry
	// // var mat = new THREE.LineBasicMaterial({
	// // 	color: 0x000000,
	// // 	linewidth: 20
	// // });
	// // var wireframe = new THREE.LineSegments(geo, mat);
	// // var lines = new THREE.WireframeGeometry(geometry);
	// // mesh.add(wireframe);
	// // move vertex
	// console.log(mesh, mesh.geometry.getAttribute('position').array);
	// setTimeout(() => {
	// 	const a = mesh.geometry.getAttribute('position').array;
	// 	for (let i = a.length - 1; i > -1; i-=3) {
	// 		if(a[i] === 0) {
	// 			continue;
	// 		}
	// 		mesh.geometry.attributes.position.set([a[i]+100], i);			
	// 	}	
	// 	mesh.geometry.attributes.position.needsUpdate = true;
	// },3000);
	// scene.add(mesh);

	//
	renderer = new THREE.WebGLRenderer({
		antialias: true
	});
	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(window.innerWidth, window.innerHeight);
	renderer.gammaInput = true;
	renderer.gammaOutput = true;
	renderer.antialias = true;
	container.appendChild(renderer.domElement);
	//

	//
	window.addEventListener('resize', onWindowResize, false);

	

	container.addEventListener('mousemove', (event) => {
		event.preventDefault();
		if(event.altKey && !altKey) {
			altKey = true;
			grid.tools.selectTool('lower');
		}
		else if(!event.altKey && altKey) {
			altKey = false;
			grid.tools.selectTool('raise');
		}
		mouse.set((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1);

		

	});
	// container.addEventListener('keydown', (event) => {
	// 	if (event.key === 'ArrowUp' || event.key === 'w') {
	// 		if (intersected && intersected.userData.isTile) {
	// 			intersected.userData.lvl++;
	// 			var geometry = new THREE.ExtrudeBufferGeometry(intersected.userData.cell.cellShape, {
	// 				...extrudeSettings,
	// 				depth: intersected.userData.lvl * 20
	// 			});
	// 			intersected.geometry = geometry;
	// 		}
	// 	}
	// 	if (event.key === 'ArrowDown' || event.key === 's') {
	// 		if (intersected && intersected.userData.isTile) {
	// 			if (intersected.userData.lvl - 1 > 0) {
	// 				intersected.userData.lvl--;
	// 				var g = new THREE.ExtrudeBufferGeometry(intersected.userData.cell.cellShape, {
	// 					...extrudeSettings,
	// 					depth: intersected.userData.lvl * 20
	// 				});
	// 				intersected.geometry = g;
	// 			}
	// 		}
	// 	}
	// });
	container.addEventListener('mousedown', () => {
	
		mouseDown = true;		
		onMouseDown();
	});
	container.addEventListener('mouseup', () => {
	
		mouseDown = false;		
	});

	container.addEventListener('keydown', (e) => {
		if(e.altKey && !altKey) {
			altKey = true;
			grid.tools.selectTool('lower');
		}	
	});
	container.addEventListener('keyup', (e) => {
		if(!e.altKey && altKey) {
			altKey = false;
			grid.tools.selectTool('raise');
		}
	});
}

function onWindowResize() {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize(window.innerWidth, window.innerHeight);
}
//
function animate() {
	requestAnimationFrame(animate);
	// controls.update();
	render();
}

function render() {
	raycaster.setFromCamera(mouse, camera);
	// console.log(scene.children)
	const intersects = raycaster.intersectObjects([...grid.grid.children, ...tiles.children], true);

	// console.log(intersects)
	// if(intersects.length > 0 && intersects[0].object.userData.isTile) {
	//   if(intersected) {
	//     if(intersected === intersects[0].object) {
	//
	//     }else {
	//
	//       intersected.material.color.setHex(intersected.userData.color)
	//       intersected = intersects[0].object
	//     }
	//     intersects[0].object.material.color.setHex( 0x24b4ff )
	//   } else {
	//     intersected = intersects[0].object
	//   }
	// } else {
	//   if(intersected) {
	//     intersected.material.color.setHex(intersected.userData.color)
	//   }
	//   intersected = null
	// }


	// intersecteds = intersects;
	if (intersects.length > 0) {
		if (intersected) {
			if (intersected === intersects[0].object) {

				// console.log(intersecteds.indexOf(intersected))
			} else {
				if (intersected.userData.onMouseLeave) {
					intersected.userData.onMouseLeave();
				}
				intersected = intersects[0].object;
				if (intersected.userData.onMouseEnter) {
					intersected.userData.onMouseEnter();
				}
			}
		} else {
			intersected = intersects[0].object;
			if (intersected.userData.onMouseEnter) {
				intersected.userData.onMouseEnter();
			}
		}
	} else {
		if (intersected) {
			if (intersected.userData.onMouseLeave) {
				intersected.userData.onMouseLeave();
			}
			grid.cursor.hide();
			intersected = null;
		}
	}
	// console.log()
	// console.log(tiles)
	// var time = Date.now() * 0.001;
	// camera.rotation.z =time * 0.5
	// mesh.rotation.z = time * 0.5;
	// mesh.rotation.x = time * 0.5;
	if (mouseDown) {
		onMouseDown();
	}
	renderer.render(scene, camera);
}
export default class Game {
	init() {
		// console.log('init');
	}

}
